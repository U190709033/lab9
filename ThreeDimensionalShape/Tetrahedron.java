package ThreeDimensionalShape;
import TwoDimensionalShape.Triangle;

public class Tetrahedron extends Triangle {
    public Tetrahedron(double side1) {
        super(side1);
    }
    public double calculatePerimeter() {
        return 3 * super.calculatePerimeter();
    }
    public double calculateArea() {
        return 4 * super.calculateArea();
    }
    public double calculateVolume() {
        return Math.pow(super.getSide(1), 3) / (6 * Math.sqrt(2));
    }
}
