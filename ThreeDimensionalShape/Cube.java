package ThreeDimensionalShape;
import TwoDimensionalShape.Square;

public class Cube extends Square {
    public Cube(double side) {
        super(side);
    }
    public double calculatePerimeter() {
        return 12 * super.getSide();
    }
    public double calculateArea() {
        return 6 * super.calculateArea();
    }
    public double calculateVolume() {
        return Math.pow(super.getSide(), 3);
    }
}
