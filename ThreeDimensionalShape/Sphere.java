package ThreeDimensionalShape;
import TwoDimensionalShape.Circle;

public class Sphere extends Circle {
    public Sphere(double radius) {
        super(radius);
    }
    public double calculateArea() {
        return 4 * super.calculateArea();
    }
    public double calculateVolume() {
        return (double)4/3 * Math.PI * Math.pow(super.getRadius(), 3);
    }
}
