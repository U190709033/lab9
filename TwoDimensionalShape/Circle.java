package TwoDimensionalShape;
import Shape.Shape;

public class Circle extends Shape {
    double radius;
    public Circle(double radius) {
        this.radius = radius;
    }
    public double calculatePerimeter() {
        return 2 * Math.PI * this.radius;
    }
    public double calculateArea() {
        return Math.PI * this.radius * this.radius;
    }
    public double getRadius() {
        return this.radius;
    }
}
