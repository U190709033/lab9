package TwoDimensionalShape;
import Shape.Shape;

public class Square extends Shape {
    double side;
    public Square(double side) {
        this.side = side;
    }
    public double calculatePerimeter() {
        return 4 * this.side;
    }
    public double calculateArea() {
        return this.side * this.side;
    }
    public double getSide() {
        return this.side;
    }
}
