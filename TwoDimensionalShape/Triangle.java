package TwoDimensionalShape;
import Shape.Shape;

public class Triangle extends Shape {
    double side1, side2, side3, height;
    public Triangle(double side1, double side2, double side3, double height) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        this.height = height;
    }
    public Triangle(double side1) {
        this.side1 = side1;
        this.side2 = side1;
        this.side3 = side1;
    }
    public double calculatePerimeter() {
        return side1 + side2 + side3;
    }
    public double calculateArea() {
        double p = calculatePerimeter() / 2;
        return Math.sqrt(p * (p - side1) * (p - side2) * (p - side3));
    }
    public double getSide(int sideNumber) {
        if (sideNumber == 1)
            return this.side1;
        else if (sideNumber == 2)
            return this.side2;
        else if (sideNumber == 3)
            return this.side3;
        else
            return 0;
    }
}
