import Shape.Shape;
import ThreeDimensionalShape.Cube;
import ThreeDimensionalShape.Sphere;
import ThreeDimensionalShape.Tetrahedron;
import TwoDimensionalShape.Circle;
import TwoDimensionalShape.Square;
import TwoDimensionalShape.Triangle;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Tester {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("instructor.txt");
        Scanner reader = new Scanner(file);
        ArrayList<Shape> shapes = new ArrayList<>();

        readFile(reader, shapes);
    }

    public static void readFile(Scanner reader, ArrayList<Shape> shapes) {
        while (reader.hasNext()) {
            String line = reader.nextLine();
            String[] letters = line.split(" ");
            for (int i = 0; i < letters.length; i++) {
                if (letters[0].equals("O") && letters[1].equals("C")) {
                    double value = Double.parseDouble(letters[2]);
                    Circle c = new Circle(value);
                    shapes.add(c);
                } else if (letters[0].equals("O") && letters[1].equals("S")) {
                    double value = Double.parseDouble(letters[2]);
                    Square s = new Square(value);
                    shapes.add(s);
                } else if (letters[0].equals("O") && letters[1].equals("T")) {
                    double[] parList = {0,0,0,0};
                    for (int k = 2; k < 6 ; k++) {
                        parList[k-2] = Double.parseDouble(letters[k]);
                    }
                    Triangle t = new Triangle(parList[0], parList[1], parList[2], parList[3]);
                    shapes.add(t);
                } else if (letters[0].equals("O") && letters[1].equals("SP")) {
                    double value = Double.parseDouble(letters[2]);
                    Sphere sp = new Sphere(value);
                    shapes.add(sp);
                } else if (letters[0].equals("O") && letters[1].equals("CU")) {
                    double value = Double.parseDouble(letters[2]);
                    Cube cu = new Cube(value);
                    shapes.add(cu);
                } else if (letters[0].equals("O") && letters[1].equals("TE")) {
                    double value = Double.parseDouble(letters[2]);
                    Tetrahedron te = new Tetrahedron(value);
                    shapes.add(te);
                } else if (letters[0].equals("TA")) {
                    double totalArea = 0;
                    for (int e = 0; e < shapes.toArray().length; e++) {
                        totalArea += shapes.get(e).calculateArea();
                    }
                    System.out.println("Total area of the shapes: " + totalArea);
                } else if (letters[0].equals("TP")) {
                    double totalPerimeter = 0;
                    for (int e = 0; e < shapes.toArray().length; e++) {
                        totalPerimeter += shapes.get(e).calculatePerimeter();
                    }
                    System.out.println("Total perimeter of the shapes: " + totalPerimeter +
                            "  //sphere doesn't have any perimeter.");
                } else if (letters[0].equals("TV")) {
                    double totalVolume = 0;
                    for (int e = 0; e < shapes.toArray().length; e++) {
                        totalVolume += shapes.get(e).calculateVolume();
                    }
                    System.out.println("Total volume of the shapes: " + totalVolume);
                } } } }
}
